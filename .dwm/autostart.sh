#!/bin/bash

# DWM Bar
#exec /home/tony/dwm-bar/dwm_bar.sh &

# DWM Status
#dwmstatus 2>&1 >/dev/null &

# DWMBLOCKS
exec /home/tony/.dwm/dwmbar &

#Keyboard Layout
setxkbmap --layout gb &

#Compositor
picom -f &

#Wallpaper
nitrogen --restore &


# Execute DWM
exec dwm

#Loop
while true;do
	dwm >/dev/null 2>&1
done



